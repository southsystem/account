package br.com.southsystem.account.service;

import br.com.southsystem.account.binders.BrokerOutput;
import br.com.southsystem.account.model.integration.LimitCreateRequest;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.stereotype.Service;

@Service
public class LimitService {

    private final BrokerOutput brokerOutput;

    public LimitService(BrokerOutput brokerOutput) {
        this.brokerOutput = brokerOutput;
    }

    public void createLimit(String id) {
        LimitCreateRequest request = new LimitCreateRequest(id);
        brokerOutput.accountCreated().send(MessageBuilder.withPayload(request).build());
    }
}
