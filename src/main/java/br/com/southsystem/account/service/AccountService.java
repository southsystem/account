package br.com.southsystem.account.service;

import br.com.southsystem.account.model.entity.Account;
import br.com.southsystem.account.model.request.CreateAccountRequest;
import br.com.southsystem.account.repository.AccountRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AccountService {

    private final AccountRepository repository;
    private final LimitService limitService;

    public AccountService(AccountRepository repository, LimitService limitService) {
        this.repository = repository;
        this.limitService = limitService;
    }

    public Account create(CreateAccountRequest request) throws Exception {
        Account account = new Account(request);

        account = repository.save(account);

        limitService.createLimit(account.getId());

        return account;
    }

    public Page<Account> findAll(Pageable page) {
        return repository.findAll(page);
    }

    public Optional<Account> find(String id) {
        return repository.findById(id);
    }

    public Account update(String id, Account account) throws Exception {
        Optional<Account> dbAccount = repository.findById(id);
        if (!dbAccount.isPresent()) {
            throw new Exception("Account not found");
        }

        Account accountDb = dbAccount.get();
        accountDb.setNumber(account.getNumber());

        accountDb.setAgency(account.getAgency());
        accountDb.setCpf(account.getCpf());

        return repository.save(accountDb);
    }

    public void delete(String id) {
        Optional<Account> dbAccount = repository.findById(id);

        if (dbAccount.isPresent()){
            Account deleteAccount =    dbAccount.get();
     deleteAccount.setActive(false);

     repository.save(deleteAccount);
        }
    }
}
