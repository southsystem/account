package br.com.southsystem.account.model.integration;

public class LimitCreateRequest {
    private String idAccount;

    public LimitCreateRequest(String idAccount) {
        this.idAccount = idAccount;
    }

    public String getIdAccount() {
        return idAccount;
    }

    public void setIdAccount(String idAccount) {
        this.idAccount = idAccount;
    }
}
