package br.com.southsystem.account.model.request;

import javax.validation.constraints.Size;

public class CreateAccountRequest {

    @Size(min = 1, max = 6)
    private String number;
    @Size(min = 4, max = 4)
    private String agency;
    @Size(min = 11, max = 11)
    private String cpf;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getAgency() {
        return agency;
    }

    public void setAgency(String agency) {
        this.agency = agency;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
}
