package br.com.southsystem.account.repository;

import br.com.southsystem.account.model.entity.Account;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends MongoRepository
        <Account, String> {
}
