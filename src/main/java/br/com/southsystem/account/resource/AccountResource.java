package br.com.southsystem.account.resource;

import br.com.southsystem.account.model.entity.Account;
import br.com.southsystem.account.model.request.CreateAccountRequest;
import br.com.southsystem.account.service.AccountService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/api/account")
public class AccountResource {

    private final AccountService service;

    public AccountResource(AccountService service) {
        this.service = service;
    }

    @GetMapping
    public Page<Account> getAll(Pageable page) {
        return service.findAll(page);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Account> get(@PathVariable("id") String id) {
        return service.find(id)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }


    @PostMapping
    public ResponseEntity<Account> create(@RequestBody @Valid CreateAccountRequest account) throws Exception {
        return ResponseEntity.ok(service.create(account));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Account> update(@PathVariable("id") String id,
                                          @RequestBody Account account) throws Exception {
        return ResponseEntity.ok(service.update(id, account));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") String id) {
        service.delete(id);

        return ResponseEntity.ok()     .build();
    }
}
