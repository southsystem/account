package br.com.southsystem.account.binders;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.MessageChannel;

@Configuration
public interface BrokerOutput {

    String ACCOUNT_CREATED = "ACCOUNT_CREATED";

    @Output(BrokerOutput.ACCOUNT_CREATED)
    MessageChannel accountCreated();
}
