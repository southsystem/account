package br.com.southsystem.account.config;

import br.com.southsystem.account.binders.BrokerOutput;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableBinding({BrokerOutput.class})
public class BinderConfig {

}
